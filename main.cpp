
// C++ program to print all primes smaller than or equal to
// n using Sieve of Eratosthenes
#include <iostream>
#include <cstring>
#include <vector>
#include <iterator>
#include <iomanip>
#include <locale>

using namespace std;

#define MAX_DISPLAY_LIMIT   30
#define DEBUG

// Comment the following line to calculate prime numbers according to user input
//#define FIND_MAX_IN_MINUTE
// if the above line is uncommented, then we need to search for the maximum number
// for which prime number calculation will take more than 1 minute.
#ifdef FIND_MAX_IN_MINUTE
    #define SEARCH_INIT_VAL     100'000'000
    #define SEARCH_STEP         10'000'000
    #define SECONDS_IN_MINUTE   60
#endif

// A utility function to call SieveOfErstosthereturn a vector of all prime numbers smaller than or equal to N and the total number of prime numbers



vector<unsigned long long> SieveOfEratosthenes(unsigned long long n, int *pTotalPrimeNumbers){
    vector<unsigned long long> PrimeNumbers;
    vector<bool> table(n, true);
    for (unsigned long long p = 2; p*p <= n; p++){
        if(table[p] == true){
            for(unsigned long long i = p*p; i <= n; i+=p){
                table[i] = false;
            }
        }
    }
    for (unsigned long long p=2; p<=n;p++ ){
        if(table[p]){
            PrimeNumbers.push_back(p);
            *pTotalPrimeNumbers += 1;
        }
    }
    return PrimeNumbers;
}

#ifdef FIND_MAX_IN_MINUTE
/*
* This function will search for the maximum number for which calculating all prime numbers
* less than it will take apporximately 1 minute (59.5 - 60.5 seconds)
* 
* If the duration for a number is less than one minute, the value will increase, otherwise it will decrease.
* The amount of increase and decrease is proportional to the difference between the one minute and the last duration.
*/
unsigned long long findMaxInMinute(void){
    unsigned long long RetVal = SEARCH_INIT_VAL;
    unsigned long long PreviousRetVal = 0, TempRetVal;
    int TotalPrimeNumbers = 0;
    clock_t start, end;
    double duration;
    bool IsUpperLimitFound = false;

    while (true)
    {
        start = clock();
        SieveOfEratosthenes(RetVal, &TotalPrimeNumbers);
        end = clock();
        duration = double(end - start) / double(CLOCKS_PER_SEC);
        #ifdef DEBUG
            cout << "Number: " << RetVal <<  " took : " << static_cast<int>(duration) << "s";
        #endif
        if(static_cast<int>(duration) == SECONDS_IN_MINUTE){
            break;
        }else{
            RetVal += ((SECONDS_IN_MINUTE-static_cast<int>(duration)) * SEARCH_STEP);
        }
        #ifdef DEBUG
            cout << " New Number: " << RetVal << endl;
        #endif
    }
    return RetVal;
}
#endif

int main(){

#ifdef FIND_MAX_IN_MINUTE
    unsigned long long MaxValue = 0;
    cout << "Searching starts with " << SEARCH_INIT_VAL << endl;
    MaxValue = findMaxInMinute();
    cout << "The code will generate prime numbers up to " << MaxValue << " in less than 1 minute."<< endl;
#else
    unsigned long long n;
    char UserAnswer = 'y';
    clock_t start, end;
    vector<unsigned long long> PrimeNumbers;
    int TotalPrimeNumbers = 0;

    cout << "\n\nEnter the number N: ";
    cin >> n;

    start = clock();
    PrimeNumbers = SieveOfEratosthenes(n, &TotalPrimeNumbers);
    end = clock();
    cout << "Total Prime Numbers in range [0.."<<n<<"] is: " << TotalPrimeNumbers << endl;
    if(TotalPrimeNumbers > MAX_DISPLAY_LIMIT){
        cout << "The list of pirme numbers is huge, do you want to see all of them? (y/n)";
        cin >> UserAnswer;
    }
    if(UserAnswer == 'y'){
        cout << "The Prime Numbers are: ";
        for (unsigned long long& it : PrimeNumbers)
            cout << it << " | ";        
    }
    // Calculating total time taken by the function.
    double time_taken = double(end - start) / double(CLOCKS_PER_SEC);
    cout << "\nTime taken by function is : " << fixed << time_taken << setprecision(10);
    cout << " sec " << endl;
    cout << "\n\n============= end of execution =========\n\n ";
#endif    
    return 0;
}
